#+AUTHOR:Xanadul
* Welcome to the TechNix!
This site is a WIP for my personal blog. Besides Linux Tech articles there will also be some of my favourite cooking recipes.
Here is a link to my [[./emacs.org][Emacs configuration]] For demonstration purposes

* Links
- [[https://fosstodon.org/@xanadul][Fediverse/Mastodon]]
- [[https://www.youtube.com/channel/UCZAohrKA6e1e6iAgwwU8cow][Youtube]]
- [[https://twitter.com/TrueXanadul][X/Twitter]]




