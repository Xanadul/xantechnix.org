#+TITLE:Linux
#+AUTHOR:Xanadul
#+STARTUP: showeverything
#+OPTIONS: toc:4

* Linux, das meistgenutzte Betriebsystem
Es stimmt, auch wenn die meisten Leute noch nie von Linux gehört haben, würde unsere Welt ohne gar nicht funktionieren. Eine unglaubliche Menge an geräten nutzt Linux. Von SmartTVs, über Traktoren, Internetroutern und Android Handys, bis hin zur gesamten Internet und den Servern auf denen es läuft. Alle nutzen nahezu immer Linux.

#+CAPTION: A Corgi image
#+ATTR_HTML: :alt corgi image :title Title! :width 200 :align left
[[../img/corgi.png]]
[[~/Pictures/Debian-OpenLogo.png]]
** Img test
Did you ever want a Corgi? No? Me neither!
But I used this good boy to test out embedding images into my website.

Nothing else to see here.

Go on!


** Aber was ist eigentlich Linux?
Genau genommen bezeichnet Linux nur den Linux Kernel, der von Linus Torvalds ursprünglich als Hobbyprojekt entwickelt wurde, aber durch seine freie Lizenz schnell grosse Beliebtheit errung und gar die Konkurrenz der UNIX Systeme verdrängen konnte.
Dieser Kernel bringt dem Nutzer allerdings nicht sehr viel, da er sozusagen nur eine ansammlung an gerätetreibern ist. Damit ein Computer genuzt werden kann, benötigt man noch ein Betriebsystem drumherum. Auf PCs und Servern wird Linux oft mit den GNU betriebsystem und weiteren tools kombiniert, allerdings gibt es auch weitere Systeme wie z.B. Busybox. In Handys wird eine abgeänderte Version vom Linux Kernel mit dem Android Betriebsystem kombiniert (Ausser natürlich bei Apple).
Wir werden uns hier Hauptsächlich mit Desktop Linux befassen, also zumeist GNU/Linux Distributionen wie z.B. Ubuntu oder Arch. Vieles über das wir hier Sprechen werden ist auch übertragbar auf Server, da diese meist ebenfalls auf GNU und Linux basieren.
** Getting Started
Der Start in die Linux Welt scheint auf Anhieb erstmal recht kompliziert, da es anders als bei Windows nicht bloss *ein* Linux gibt, sondern tausende sogenannte Distributionen. Die meisten davon unterscheiden sich zum Glück aber nur im Detail voneinander. Ein wichtiges Detail ist allerdings die sogenannte Desktop Umgebung (engl.: Desktop Environment, abgekürzt DE), welche bestimmt wie ihr system aussieht und sich bedienen lässt.

*** Wahl der Desktop Environment
Da die DE massgeblich beeinflusst wie sie mit ihrem computer interagieren kann man argumentieren, dass sie eine wichtigere Wahl darstellt als die der Distribution. Ausserdem ist es meiner Meinung nach auch die Spannendere Wahl. Dementsprechend rate Ich auch, zuerst die DE zu wählen, und dann nach einer Distro zu schauen welche diese DE vorinstalliert anbietet.
In [[./desktopEnvironment][diesem Artikel (Soon)]] stelle Ich einige DEs vor, um ihnen bei der Wahl zu helfen.

*** Wahl einer Distribution
Wie bereits erwähnt gibt es viele Distros, und es gibt auch nicht "die beste". Manche eignen sich besser für Software Entwickler, andere für Bastler und lernbegierige, und andere wiederum besser für alltägliche Nutzer. Und auch dort kann man nirgends von "der besten" reden, da jede distro doch ihre einen vor und nachteile für jeden individuellen Nuzter hat. Daher biete Ich [[./distro][hier (Soon)]] lediglich eine grobe Übersicht über die paar wichtigsten Distros, die als Startpunkt dienen können um die eigenen Favoriten zu finden.

** Basics
Sie haben ihr System erfolgreich Installiert und möchten nun direkt anfangen? Ein guter Anfang kann es sein, einmal durch das Einstellungs Programm durch zu schauen. Optionen wie das Hintergrunbild, wahl zwischen Hellen und Dunklen Farben, Wlan Verbindung, Mausgeschwindigkeit und Scrollrichtung stellen wohl die wichtigsten Einstellungen dar.

*** Neue Programme installieren
Vermutlich möchten sie schon bald ein neues Programm installieren, aber *ACHTUNG*, in Linux geht dies etwas anders (leichter) als sie es vermutlich von Windows gewohnt sind.
Lesen sie sich am besten [[./basics/addingsoftware][diesen Artikel]] von mir durch.

*** Software Empfehlungen
Nicht jedes programm, das sie in Windows oder MacOS nutzten gibt es auch in Linux. Das muss aber auch nicht schlimm sein!
Wir haben viele Alternativen für fast jedes Programm, das sie villeicht nutzen wollen. Zu aller meist gratis von der Community Entwickelt, oft können sie mit dem Industriestandart mithalten oder sie sind sogar der Industriestandart. In [[./software][diesem Artikel]] (ComingSoon!) stelle ich meine Favoriten vor!
 
*** Grundlagen des Terminals
Coming Soon!

** Fortgschritteneres
Wenn sie die Basics gelernt haben, und noch mehr lernen möchten, sind Hier einige weitere Themen die sie interessieren könnten
*** Window Manager
Soon

*** Hyprland
Soon

*** InitSystem
NotSoSoon

*** Emacs und Vim
Soon

*** Praktische Terminal Software 
WIP

*** Dotfile (User Config) Management
Sometime in the future

*** "Echtes" Linux auf dem Handy  
PostmarketOS: When I get around to it

