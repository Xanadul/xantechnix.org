#+TITLE: StableDiffusion Basics and Advanced
#+AUTHOR: Xanadul
#+STARTUP: showeverything
#+OPTIONS: toc:4

* How StableDiffusion works
Stable Diffusion is a neural network used as a txt2img and img2img AI, based on the principle of noise diffusion. This basically means, that it iteratively tries do denoise an image, sharpening the objects defined by the user in the prompt.
** Internal Workflow
This just an overview of what is happening inside the AI. While not neccesarry to know in order to use it, It's still good to know, especially since I'll reference it later a bit.
*** 1. Textual Encoding (Tokenizing)
When inputting a prompt (i.e. an instruction of what the AI should generate), a part of SD called the Textual Encoder tokenizes the prompt, meaning it cuts words into 1-4 letter sized chunks which the actual working model can understand. The popular version 1.5 of Stable Diffusion can take 75 such tokens as input, however, modern UIs like Automatic1111 can combine tokens to drastically increase the amount of tokens which can be used.

*** 2. Generating Latent noise
After the Textual Encoding is done, it continues with creating a noisy image, based on a seed number (kinda similar to Minecraft world generation). This noise image is created in latent space which basically is a special kind of image SD uses internally (oversimplified). The Latent resolution is lower than the resolution of the output image. Keep that in mind, whenever we talk about something latent.

*** 3. Denoising
Now, the actual diffusion model starts to work. It uses the tokens of the prompt to denoise this noise into what it "recognizes" within the noise as your prompt. This is called a step. The output is then fed back into the Diffusion Model, which then repeats the process (doing a second step). To get decent results, you usually need at least 15 steps, but rarely more than 25.
**** Sampling Method
The denoising also uses a Sampling Method, which is basically a small programm influencing the denoising. Different samplers can result in different results.

*** 4. Decoding
Up until now, the image has been in latent space, which still needs to be converted into a standart .png image. This is what the Decoder is doing. To do so, it should use a VAE (Variational Auto-Encoder). Many Models come with a VAE baked in, but others don't. VAE can also influence details and color of the resulting image. If your images come out desaturated, SD is most likely not using a VAE.

** Model, Lora, Embedding, Hypernetwork, Lycoris
These are all trained models which influence the result of your generations.
*** Model/Checkpoint
The most important one! The model includes the Textual Encoder (i.e. the prompts the AI can "understand") and defines the artstyle of the images. There are two categories in which we can put each model:
**** SD-Version
Categorizes on which version of Stable Diffusion the model is based. Most common is SD1.5, which is trained using 512x512p images, and therefore excels at them. SD2.0 is trained on higher resolutions, but is unpopular due to the inclusion of censoring within the models (e.g. it won't create 18+ content).
At the time of writing, SDXL 1.0 is pretty new. It's trained on 1024x1024 images, needs less prompts, generates better hands, but needs more VRAM by default and does not (yet) have decently trained Anime models.
**** Anime vs. Realism
The original SD-Model trained by StabilityAI is a realistic model. This does not only mean that it creates realistic looking images, but also that *it needs prompts written in natural language!*
A prompt for a realistic model could look like this:
#+begin_src 
complex 3d render ultra detailed of a beautiful porcelain profile woman android face, cyborg, robotic parts, 150 mm, beautiful studio soft light, rim light, vibrant details, luxurious cyberpunk, H. R. Giger style, an extremely delicate and beautiful
#+end_src 

On the other hand, we have Anime Models. They not only differ in artstyle, but in usage too! When prompting, it is recommended to use tags found on sites like danbooru (warning, nsfw content).
A typical prompt would look like this:
#+begin_src
masterpiece, best quality, 1girl, long hair, white hair, blurry foreground, white dress, full body, outdoors
#+end_src

Usually saved as a .safetensors or sometimes .ckpt

*** LoRA
LoRA (Low-Rank Adaptation) are trained NeuralNetworks, which you can "add into" your model to help StableDiffusion understand new Concepts. You can basically loosely categorize them into 4 diffent types (At least, thats how I would do it):
- Objects/Characters/Clothing, e.g. DanielCraig or Bananas
- Styles, e.g. the style of a certain artist
- Actions/Poses, e.g. Eating a large Cheeseburger, or Riding a Chocobo
- Specials, e.g. A lora for adding/removing details
To use a Lora, you need to do two things: Load it and trigger it. Some lora don't need to be triggered, usually style-lora.
They are usually .safetensors

**** Technical explanation
Will follow... I'm not really an expert in this yet, but its not really needed to understand how they work to use or even create them.

*** Hypernetworks/LyCORIS
For users, these two work the same as LoRA. Similar usage, same effects.
As a rule of thumb (based on my opinion): Hypernetworks are worse than LoRA, but LyCoris are usually better.
They are usually .safetensors

*** Embeddings/Textual Inversion
Embeddings aka. Textual Inversions are similar to LoRA too, but are used a bit differently. They usually only need to be triggered.
For concepts like Objects,Characters,Poses I would not recommend to use Embeddings, as they don't work that well for that. What they absolutely do excell at is controlling quality. E.g. there are some Embeddings which will drastically improve the look of generated hands. Instead of looking like some mutations, they will tend to look more like actual hands, sometimes even with 4 fingers + a thumb!
They are usually saved as a .pt file
* Using Automatic1111
The Automatic1111 UI is the most well known UI for running SD on your own hardware. It's packed full of features and can even be expanded by using extensions. The [[https://github.com/AUTOMATIC1111/stable-diffusion-webui/wiki][official wiki]] of the UI features installation instructions for Win/Mac/Linux and AMD/Nvidia/Apple/Intel GPUs. It also includes a lot of other information and can definetly be worth a look.
In Linux, installation basically comes down to:
- 1. Installing python3.10 and its venv module
- 2. Cloning the UIs git repo
- 3. Creating a venv called venv in the cloned repo
- 4. Running the webui

Regarding step 4, when starting the webui with "./webui.sh", It's worth knowing a few extra arguments.
| Argument                           | Effect                                                                                          |
|------------------------------------+-------------------------------------------------------------------------------------------------|
| --listen                           | Opens the Ui to the local network, so that you can access it from another computer              |
| --lowvram                          | Reduces the amount of vram needed for generating images by a lot (also reducing speed)          |
| --medvram                          | Reduces the amount of vram needed by a bit (marginal speed loss)                                |
| --xformers                         | Enables xformers on Nvidia GPUs, speeding up generations, but making them non deterministic     |
| --enable-insecure-extension-access | See below, in "Extensions"                                                                      |
| --no-half-vae                      | needed on some GPUs to force 32bit floating point mode, since some don't support fp16 (most AMD) |
 ‌ ‌‌
As a reference for VRAM usage, on my 8GB Vega 56 I'm usually using --medvram and am able to generate at 

Once the terminal says something like "Running on local URL:  http://0.0.0.0:7860" you can connect to the UI on that device following the link. If using --listen, you can also connect from another device in the same network, using the local IP of the device the UI is running on (e.g. http://192.168.0.50:7860 ). You can easily determine your local ip with the command "ip a" in Linux (and probably MacOS) terminal or "ipconfig" in Windows CMD/Powershell.
** Important Settings
Alright, now lets look at the UI and all its textfields, sliders and buttons, one by one.
*** txt2img tab
In this tab, you can create images from a text, which is usually called text to image, or shortened to txt2img.
**** Stable Diffusion checkpoint
A basic list to choose the actual model that gets used during the denoising. For a better (visually pleasing) way to set your Checkpoint, take a look at "Using additional Networks" further down.
**** Prompt and Negative Prompt
Here you can add the instructions, which the TextEncoder will tokenize and pass to the Model. Whatever you write in the prompt will (hopefully) end up in your image. Example:
#+begin_src 
masterpiece, 1girl, Hatsune Miku, red shirt, (black hair:1.5) 
#+end_src
Adding round brackets (or parenthesis as Americans call them) emphasizes their content. Nesting brackets in brackets (((like this))) increases emphasis. Alternatively, you can specify emphasis directly using colon and number (like this:1.5)
We emphasize black hair, because the AI really wants to give Miku her blue haircolor.

Things you add in the Negative prompt will get avoided in the Diffusion process, and should (hopefully) not end up in the image. Example:
#+begin_src 
(low quality, worst quality:1.3) 
#+end_src
**** Sampling Method
The sampler, which will get used during the denoising. Personally, I usually like "DPM++ 2M SDE", but test for yourself. Some prompts and models work better with different samplers.
**** Sampling Steps
How many iterations of denoising should be done. 15-25 are usually sane values, but this can vary depending on the model and the sampling method
**** Highres Fix
A way to generate images at higher resolutions, without duplication artifacts. When using this, your image will get generated as usual, but after the set amount of steps, it will upscale the image, add a bit of noise and repeat the denoising. "Denoising Strenght" is probably the most important setting here as it determines how much the upscaled image will differ from the original. Below 0.3 will usually result in a washed out image.
Using upscale factors higher than 2 tends to create more wierdness, like extra arms or heads (in that case, try reducing the denoise strenght).
**** Width/Height
The resulutions of the final image (or when using Highres Fix, the resolutions before upscaling)
Stable Diffusion 1.5 (which is the most popular version) is trained on 512x512 images, and is best at this resolution. 512x768 and 768x768 can still give decent results too. Some models are good at using 1024x1024 too, but it's a bit rare.
*Do note* that increasing the resolution exponentially increases needed VRAM too!
**** Batch count/size
You can create images in batches. Batch Size determines how many images are created in paralell, Batch count how many of those batches should be created. They will usually start with the set Seed and increase it by one for each image. Useful when testing out prompts, but Batch Size lineary increases needed VRAM.
**** CFG Scale 
ContactFreeGuidance determines how closely the AI should follow your instructions. Lower values increase it's "creativity", higher ones decrease it. 7 is a sane value, but lowering it to something like 3 can create really interesting images too.
**** Seed
This number determines the Random Noise image that gets created as a latent (before the Denoising starts). Using a set seed should result in similar images. E.g. When setting the seed as 774, and creating an image with the prompt "1girl, hatsune miku" and one with "1girl, hatsune miku, sunglasses" should result in the same pose and framing, but one with sunglasses, the other without. So this can be used for finetuning your prompt and settings.
A seed of -1 means, that the seed is randomly choosen.
**** Script
Scripts are very powerful in a variety of different fields. I've only used the X/Y/Z-Plot till now, which is a powerful way of generating grids of images which differ in single settings from each other.
Here is an example:
- X type: CFG Scale
- X values: 1,2.5,4,6,8,10,16
- Y type: Prompt S/R (Seek/Replace)
- Y values: sunglasses, glasses, round glasses
This will create 7*3 (=21) images in a grid. The Prompts S/R will seek for the word "sunglasses" in the prompt and replace it with "glasses" and "round glasses" on the Y axis.
Using this X/Y plot should give a good understanding of the CFG Scale setting.
I recommend creating a few plots with HighresFix upscale and denoising, sample size and scheduler to gain a deeper understanding of these values.


*** Extras Tab
This is usually used for upscaling created images using different img2img AIs trained on upscaling. You can set a scale factor, choose one or two upscalers (which will be blended together) etc.

*** PNG Info Tab
By default, every image you generate will have your generation settings saved in the .png metadata. In this tab, you can paste an image and see your used generation settings. Images shared on Twitter, Whatsapp, Pixiv, etc. will usually get stripped of their metadata, rendering this tab mostly useless for these images. Images from Civitai usually include metadata.
You can also send the selected image and prompt directly to txt2img, img2img, etc, which is super convenient when you want to recreate an image or want to continue working on your settings another day.

*** Checkpoint Merger, Train
This is used for creating your own Models or merging Several models together. I have no experiance with this tab.

*** Text2Prompt
Well the name says what you can do here, but I havn't used it yet, so I can't comment on it.

*** Settings
Here you can customize pretty much anything. Saving behavior, live previews, UI Reorders, etc.
Some settings I recommend:
- Saving images/grids:
  - Save copy of image before highres fix: true
  - Save text information about generation ... to png files: true (Ethical and ensures reproducability, see PNG Info Tab)
  - Create a text file next to image with generation parameters: true
- User interface:
  - Gradio Theme: choose your favourite colorscheme for the WebUI
  - Quicksettings list: sd_model_checkpoint sd_vae CLIP_stop_at_last_layers (adds quicksettings for CLIP and VAE to the top)
- Live previews:
  - Live preview display period: 2 (gives some nice visual feedback, but slows generation down by a bit)

** Using Additional Networks (LoRA, LyCORIS, Embeddings, etc)
*** Installing
- Models: Download into: "[WebUIBaseDir]/models/Stable-diffusion/" 
- Lora: Download into: "[WebUIBaseDir]/models/Lora/"
- Lycoris: Download into: "[WebUIBaseDir]/models/Lora/" (yes, in up-to-date versions, its the same as Lora)
- Hypernetworks: Download into: "[WebUIBaseDir]/models/hypernetworks/"
- Embeddings: Download into: "[WebUIBaseDir]/embeddings/" (yes, not in the models directory for some reason...)

Personally, I recomend to add an image and optionally a textfile of the same names for each downloaded file. The image should be representative of the effects of the Lora, as it will be used as a thumbnail in Automatic1111. In the textfile I would add the trigger word and weight (for Lora/Lycoris/Hyper), additional information and a link to the download page. This will show up in the WebUI too, but there is a new and better way to do so (See below).
For example, if you add my EliaStellaria Lora, you would have these files:
"[WebUIBaseDir]/models/Lora/EliaStellaria.safetensors"
"[WebUIBaseDir]/models/Lora/EliaStellaria.png/jpg"
"[WebUIBaseDir]/models/Lora/EliaStellaria.txt"

*** Usage
Inside the WebUIs txt2img and img2img tabs, you can change the tab from "Generation" to "Lora" and the other ones.
This is where we will see the images we saved (try the refresh button if you can't find new additions). If you added .txt files too, you will be able to see their contents too (hover with mouse to expand).
On click, it will add the networks activation to the prompt and will look like this <lora:Elia:1>
On hover, you will get a Tool button, try it out! A window will open, allowing you to Edit the description (Which is the content of the .txt), see the training data Tags the creator used, set a new thumbnail and most importantly:
**** Activation Text and weight
You can (and probably should) add the main trigger word here, and set the default weight of the Network. This will automatically add the trigger word(s) along with the activation to the prompt when you click the networks entry.
If the Network has optional prompts (e.g. outfit prompts for a Character), I would add it to the notes instead, but its your choice.

**** Embeddings
Embeddings are used differently. As long as you have saved them in the correct directory, you can simply add them by their name into the promt. E.g.: For the awesome [[https://civitai.com/models/7808/easynegative][EasyNegative Embedding]], just add "EasyNegative" to your negative prompt.

*** Organization
You don't have to put all your Lora into the same folder. You can use subdirectories, which will help you a lot if you have many Lora or maybe want to seperate NSFW Lora from SFW Lora. Same goes for Checkpoints, Lycoris, etc.
Eg. My Lora directory tree looks like this:
#+begin_src sh 
Lora
├── NSFW
│   ├── NClothes
│   ├── NPoses
│   ├── NSpecial
│   └── NStyle
└── Safe
    ├── Characters
    │   └── Elia
    │       ├── New
    │       └── Stanana
    ├── Clothes
    ├── Objects
    ├── Poses
    ├── Special
    └── Style
        └── ObjectStyles
#+end_src

The reason I prepended the NSFW folders is an old bug (thats maybe fixed by now) merging Safe/Clothes with NSFW/Clothes in the WebUI. Same reason for why I use SFW instead of NSFW.
For Checkpoints, I use this Structure:
#+begin_src sh 
Stable-diffusion
├── Anime
├── AnimeXL
└── Realism
#+end_src
*** Networks I recommend
Checkpoints:
- [[https://civitai.com/models/130160/sakuramix][Sakuramix V4.0]] 
- [[https://huggingface.co/NoCrypt/SomethingV2_2][Someting V2.2]] 
- [[https://civitai.com/models/9409?modelVersionId=11162][Anything v3.0]] (Newer versions exist on the same page, check them out too)

Lora:
- [[https://civitai.com/models/13941/epinoiseoffset][EpiNoiseOffset]] Adds deep shadows and bright highlights to your image
- [[https://civitai.com/models/22470/gundam-rx78-2-outfit-style-rx78-2][Gundam RX78-2 outfit]] Ever wanted a Girl dressed as a Gundam? It's fantastic
- [[https://civitai.com/models/22977?modelVersionId=27658][Flat Style]] Reduces 3D effect of several anime models
- [[https://civitai.com/user/konyconi/models][Konyconis Styles]] They have dozens of cool style lora, drastically changing the genre of your creations
- [[https://civitai.com/models/108401/eliastellaria-vtuber-lora][EliaStellaria]] Yeah, shameless plug of my own Lora XD

Embedding:
- [[https://civitai.com/models/7808/easynegative][EasyNegative]] A must have negative embedding (use in negative prompt) to increase image quality
- [[https://civitai.com/models/4629?modelVersionId=5637][DeepNegative]] Mostly an alternative to EasyNegative
- [[https://civitai.com/models/7808/easynegative][BadHandV4]] Put in negative promp: Get way better hands


** Extensions
Now that we took a look at the standard UserInterface of Automatic1111, we will take a look at adding even more features bit by bit.
We will use Extensions for that. To install extensions, you first have to start the UI with an extra argument in the terminal/cmd
#+begin_src sh
sh ./webui.sh --enable-insecure-extension-access
#+end_src
This is only needed for installing extensions, not for running them. So I recommend to not use this argument when using the UI, just for installing/updating the extensions. The reason being: it's insecure. Especially when combined with the --listen argument, because then someone in you Wifi could basically install a virus into your Automatic1111 UI. Also, in the same train of thought: Only install extensions you trust. High activity and many stars on its linked git repository can be a good indicator for trustworthyness, but are not perfect!

So, to proceed the Installation:
In the WebUI, click on the Extensions tab, then the "Avaliable" tab. The textfield next to the "Load from:" button should already contain a link to the official extensions list. If it doesn't copy/paste it yourself: https://raw.githubusercontent.com/AUTOMATIC1111/stable-diffusion-webui-extensions/master/index.json
Now you can click the button and will get a list of all these extensions. For your first visits, It's a good idea to sort by stars, which will order by popularity. To install a extension, click its install button, return from the "Avaliable" tab to the "Installed" tab and click on "Apply and restart UI"
Done!
So, here are my personal favourites:
*** a1111-sd-webui-tagcomplete
This will add a small popup when typing your prompt, suggesting booru style tags, for your Anime images. Very handy, as Anime models are trained to understand exactly these tags.
*** multidiffusion-upscaler-for-automatic1111
Ever got a "CUDA out of memory" error? This usually happens when your choosen resolution is so large, that you need more VRAM in your Graphics Card. Sadly, you can't add more VRAM without replacing the entire graphics card. Using --verylowvram makes generations abyssmally slow, but this extension is your saviour!
In the txt2img and img2img tabs, you will get 2 new sections:
**** Tiled VAE
If you get the out of memory errors at 99%, that is no coincidence. The Decoding using the VAE usually needs more VRAM than the generation itself. And since the decoding is the very last step, this wastes all the time you waited for your image.
With Tiled VAE, you can fix this issue. Just enable it and disable the Fast Encoder *OR* enable the "Fast Encoder Color Fix". For very limited VRAM, you might want to reduce the Encoder and Decoder Tile Sizes. It is normal for the Decoder Tile Size to be significantly smaller than the Encoder one. E.g. 1536/96 is usually good for my 8GB Graphics Card.
There is basically no conceivable downside to using Tiled VAE, I always enable it to avoid possible errors at 99%

**** Tiled Diffusion
If you get out of memory errors during creation, especially at 50% when using Highres Fix, you can enable Tiled Diffusion. It will cut your image into smaller images (tiles) and work on 1 to 8 tiles at a time (set using "Latent tile batch size"), *drastically* reducing VRAM usage.
As a rule of thumb, you might want to set the Tile width/height to about a tenth of your output resolution (If using Highres fix, use its output resolution).
Overlap of 50% is a good idea, if set too low you will get visible seams in the output image, if set too high you will waste a lot of time.
You can choose the method to your own preference. Try both out and decide for yourself which you prefer.

If you are in the img2img tab, you also get another section within Tiled Diffusion: Noise Inversion
This is basically an upscaling method which can also add more detail into the image.
When you enable Noise Inversion, you should reduce Denoising Strenght (in the settings further up) to below 0.6
Feel free to experiment however. Also, set an Upscaler and a Scale Factor within Tiled Diffusion, even something like 4 or higher is fine.

* General Advice
If Crashing due to OutOfMemory erros, use TiledDiffsion (See above) and close as many applications which could use your GPUs VRAM as possible. For me, this includes my web browser, video player, gpu accelerated terminal, etc... One kinda drastic measure is closing my entire GUI too, by stopping my Login Manager (aka Display Manager), which is probably impossible in Apple and Windows Dektops (For windows server edition it might be possible, idk.). I can start the webui with --listen in my TTY instead of a terminal window, and use my laptop to access the WebUI. Drastic measures, but can free up a few hundred MiB from VRAM.



* My Usual Workflow
All of this takes some time, especially on slower GPUs, but results in super high resolution images and content I'm mostly contend with.
Each step can be repeated until I'm happy with it's result:
** Getting an Idea
Not strictly neccessarily an active process. Maybe something unexpected happens during another image generation, I see something inspiring or just find a new Lora on civitai I wann try. Even just inputting a basic prompt and expanding it each generation can work.
** Engineering a basic prompt 
As a start, I build a basic prompt, which should give me images similar to what I want to generate.
** Generate a few batches
Generating images at the base size I want to use, to check the prompts usefulnes and maybe even select a Seed.
** Generate a X/Y/Z plot
Using several Checkpoints on one axis, maybe a Prompt S/R to try out a few variations and some seeds I liked on the last axis. Can give a good overview and help find the Checkpoint and seed, but can take a long time.
** Generate the HighRes Fix
Taking my favourite combinations from the XYZ plot and generating them with HighRes Fix, usually doubling Resolution.
** Fine tuning the best one
Change some settings and maybe prompt, regenerate image with highresfix a few times until the bes one was found.
** Use Tiled Diffusions Noise Inversion
This step takes a bit of time and is risky on an 8GB GPU, but can x4 or maybe even x8 the resolution. This results in x8 or even x16 resolution of the output image, because of using Highres Fix before.
I could maybe use Upscaling from the extras tab, but because of Noise Inversion, the resolution usually exceeds 4k anyway.
